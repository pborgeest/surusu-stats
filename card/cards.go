package card

import "time"

type Snapshot struct {
	Timestamp time.Time // UTC
	Decks     []string
}

type Cards struct {
	Deck       string
	Cardsdue   int
	Cardstotal int
}


