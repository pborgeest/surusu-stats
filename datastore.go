package gimpledred

import (
	"appengine"
	"appengine/datastore"
	"bitbucket.org/pborgeest/gimpledred/card"
	"time"
)

type FullSnapshot struct {
	key      *datastore.Key
	snapshot card.Snapshot
}

type ConfigStore struct {
	Key   string
	Value string
}

func GetConfig(c appengine.Context, key string) (string, error) {
	q := datastore.NewQuery("ConfigStore").
		Filter("Key =", key)
	var configs []ConfigStore
	_, err := q.GetAll(c, &configs)
	if err != nil {
		return "", err
	}
	if len(configs) < 1 {
		err := putConfig(c, key)
		return "", err
	}
	return configs[0].Value, err
}

func putConfig(c appengine.Context, key string) error {
	k := datastore.NewIncompleteKey(c, "ConfigStore", nil)
	_, err := datastore.Put(c, k, &ConfigStore{key, ""})
	return err
}

func putTheseDecksInStorage(c appengine.Context, decks []string) (int64, error) {
	k := datastore.NewIncompleteKey(c, "Snapshot", nil)
	key, err := datastore.Put(c, k, &card.Snapshot{time.Now(), decks})
	return key.IntID(), err
}

func getSnapshotKey(c appengine.Context, id int64) *datastore.Key {
	return datastore.NewKey(c, "Snapshot", "", id, nil)
}

func putTheseCardsInStorage(c appengine.Context, rootkeyid int64, cards card.Cards) error {
	k := datastore.NewIncompleteKey(c, "Cards", getSnapshotKey(c, rootkeyid))
	_, err := datastore.Put(c, k, &cards)
	return err
}

func getThisWeek(c appengine.Context) ([]FullSnapshot, error) {
	q := datastore.NewQuery("Snapshot").
		Filter("Timestamp >", time.Now().AddDate(0, 0, -7)).
		Order("Timestamp").
		Limit(14)
	var dst []*card.Snapshot
	keys, err := q.GetAll(c, &dst)
	if err != nil && !isErrFieldMismatch(err) {
		return nil, err
	}
	fullsnapshots := make([]FullSnapshot, len(dst))
	for i, snapshot := range dst {
		fullsnapshots[i] = FullSnapshot{
			keys[i],
			*snapshot,
		}
	}
	return fullsnapshots, nil
}

func getThisDecksTotalCards(c appengine.Context, ancestor *datastore.Key, deckname string) int {
	q := datastore.NewQuery("Cards").
		Ancestor(ancestor).
		Filter("Deck =", deckname)
	var cards []card.Cards
	q.GetAll(c, &cards)
	for _, k := range cards {
		return k.Cardstotal
	}
	return 0
}

func getThisDecksDueCards(c appengine.Context, ancestor *datastore.Key, deckname string) int {
	q := datastore.NewQuery("Cards").
		Ancestor(ancestor).
		Filter("Deck =", deckname)
	var cards []card.Cards
	q.GetAll(c, &cards)
	for _, k := range cards {
		return k.Cardsdue
	}
	return 0

}

func getCardsFromSnapshot(c appengine.Context, ancestor *datastore.Key) []card.Cards {
	q := datastore.NewQuery("Cards").
		Ancestor(ancestor).
		Order("Deck")
	var cards []card.Cards
	q.GetAll(c, &cards)
	return cards
}

// From http://code.google.com/p/appengine-go/source/browse/appengine/blobstore/blobstore.go
func isErrFieldMismatch(err error) bool {
	_, ok := err.(*datastore.ErrFieldMismatch)
	return ok
}
