package gimpledred

import (
	"net/http"
)

func init() {
	http.HandleFunc("/cron", cronhandler)
	http.HandleFunc("/fetchonedeck", fetchdeckhandler)
	http.HandleFunc("/", graphhandler)
}

