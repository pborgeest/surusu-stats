
package gimpledred

import (
	"appengine"
	"appengine/memcache"
	"text/template"
	"fmt"
	"net/http"
	"bitbucket.org/pborgeest/gimpledred/card"
	"bitbucket.org/pborgeest/gimpledred/surusu"
)

var (
	templates = template.Must(template.ParseFiles("root.tmpl"))
)

func graphhandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	viewModel, err := getTheViewModelFromCache(c) 
	if err != nil {
		snapshots,err := getThisWeek(c)
		if err != nil {
			fmt.Fprintf(w, "Error: %v", err)
			return
		}
		viewModel = getViewModel(c, snapshots)
		c.Infof("Generated Viewmodel: %v\n", viewModel)
		setTheViewModelInCache(c, viewModel)
	}
	// end 
	templates.ExecuteTemplate(w, "root.tmpl", viewModel)
	return
}

func getDeckMap(client *http.Client, decks []string) map[string]card.Cards {

	deckmap := map[string]card.Cards{}
	for _, deck := range decks { 
		deckcards, _ := surusu.GetDeckCards(deck, client, len(decks)+2)
		deckmap[deck] = deckcards
	}
	return deckmap
}

func getTheViewModelFromCache(c appengine.Context) (ViewModel, error) {
	var output ViewModel
	_, err := memcache.Gob.Get(c, "viewmodel", &output)
	return output, err
}
func setTheViewModelInCache(c appengine.Context, viewModel ViewModel) {
	item := &memcache.Item{
		Key: "viewmodel",
		Object: viewModel,
	}
	memcache.Gob.Set(c, item)
}

