// +build !appengine

package main

import (
	"net/http"
	"net/url"
	"io/ioutil"
	"fmt"
	"log"
	"errors"
	"strings"
	//"strconv"
	"sort"
)


type Jar struct {
	cookies []*http.Cookie
}

func (jar *Jar) SetCookies(u *url.URL, cookies []*http.Cookie) {
	jar.cookies = cookies
}

func (jar *Jar) Cookies(u *url.URL) []*http.Cookie {
	return jar.cookies
}

func newClient() *http.Client {
	jar := new(Jar)
	return &http.Client{nil, nil, jar}
}

func main() {

	client := newClient()

	if foundLoginForm(client) {
		login(client)
	}

	decks := getDecks(client)
	deckmap := map[string]int{}
	total := 0
	for _, deck := range decks {
		deckcards, _ := getDeckCards(deck,client, 2 /*len(decks)*2*/)
		deckmap[deck] = deckcards
		fmt.Println(deck, ": ", deckcards)
		total += deckcards
	}
	fmt.Println(deckmap)
	fmt.Println("Total:",total)
}

func getDeckCards(deck string, client *http.Client, limit int) (int,error) {
	if limit < 0 {
		return 0, errors.New(fmt.Sprintf("Deck '%s' not found\n", deck))
	}
	response, err := client.Get("http://surusu.com/stats.php")
	//_, err := client.Get("http://surusu.com/switchdeck.php?gotonextdeck=true")
	if err != nil {
		return 0, err
	}
	body := getBody(response, err)

	found := false
	for _, line := range strings.Split(body, "\n") {
		if strings.Contains(line, fmt.Sprintf("Current deck: %s", deck)) {
			found = true
		}
	}
	if !found {
		fmt.Println("not found - trying to post")
		values := make(url.Values)
		values.Set("selecteddeck", deck)
		values.Set("switchdeck", "true")
		values.Set("referringpage", "/stats.php")

		fmt.Println("POSTing: ", values)
		resp, err := client.PostForm("http://surusu.com/switchdeck.php", values)
	if err != nil {
		fmt.Println("Error: ", err)
		return 0, err
	}
//		_ = getBody(resp, err)
		fmt.Println("RESPONSE: " , getBody(resp, err))

	response, err := client.Get("http://surusu.com/index.php")
	_ = getBody(response, err)
	if err != nil {
		fmt.Println("Error: ", err)
		return 0, err
	}
	response, err = client.Get("http://surusu.com/stats.php")
	//_, err := client.Get("http://surusu.com/switchdeck.php?gotonextdeck=true")
	if err != nil {
		fmt.Println("Error: ", err)
		return 0, err
	}
	body = getBody(response, err)
		for _, line := range strings.Split(body, "\n") {
				fmt.Println("Checking: ", line)
			if strings.Contains(line, fmt.Sprintf("Current deck: %s", deck)) {
				found = true
			}
		}
		if found {
			fmt.Println("FOUND")
		} else {
			fmt.Println("NOT FOUND")
	//		fmt.Println(body)
		}

		//return getDeckCards(deck, client, limit-1)
	}

	/*
	response, err := client.Get("http://surusu.com/question.php")
	body := getBody(response, err)

	found := false
	for _, line := range strings.Split(body, "\n") {
		if strings.Contains(line, fmt.Sprintf("DECK:::%s", deck)) {
			found = true
		}
	}
	if !found {
		return getDeckCards(deck, client, limit-1)
	}

	res, err := client.Get("http://surusu.com/stats.php")
	body = getBody(res, err)
	for _, line := range strings.Split(body, "\n") {
		if strings.Contains(line, "Duecount:") {
			firstsplit := strings.Split(line, "<strong>")
			secondsplit := strings.Split(firstsplit[1], "</strong>")
			cards, _ := strconv.Atoi(secondsplit[0])
			return cards, nil
		}
	}
	*/
	return 0, nil
}

func getDecks(client *http.Client) []string {
	res, err := client.Get("http://surusu.com/stats.php")
	body := getBody(res, err)

	foundSwitchdeck, foundReload := false, false
	decks := make([]string, 0)
	for _, line := range strings.Split(body, "\n") {
		if foundSwitchdeck && !foundReload {
			if strings.Contains(line, "<option value=\"") {
				optionsplit := strings.Split(line, "\"")
				decks = append(decks, optionsplit[1])
			}
		}
		if strings.Contains(line, "\"switchdeck.php\"") {
				foundSwitchdeck = true
		}
		if strings.Contains(line, "\"Re/Load Stats\"") {
				foundReload = true
		}
	}
	sort.Strings(decks)
	return decks
}

func login(client *http.Client) {
	_, err := client.PostForm("http://surusu.com/login.php", url.Values{"username": {"Fepelus"}, "password": {"maralinga"}})
	if err != nil {
		log.Fatal(err)
	}
}

func foundLoginForm(client *http.Client) bool {
	res, err := client.Get("http://surusu.com/")
	body := getBody(res, err)
	return strings.Contains(body,"form method=\"POST\" action=\"login.php\"") 
}

func getBody(resp *http.Response, err error) string {
	if err != nil {
		log.Fatal(err)
	}
	robots, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	return string(robots)
}



/*

<FORM ACTION="switchdeck.php" METHOD="POST">
<SELECT NAME="selecteddeck" onchange="this.form.submit();">
<OPTION VALUE="italian"/>Current deck: italian							<font size="10">
<option value="italian"/>italian				</font>
<font size="10">
<option value="randoms"/>randoms				</font>
<font size="10">
<option value="russian"/>russian				</font>
<font size="10">
<option value="srs.txtsrs.txt51497f43b43"/>srs.txtsrs.txt51497f43b43				</font>
<font size="10">
<option value="commonplace"/>commonplac				</font>
<font size="10">
<option value="music"/>music				</font>
<font size="10">
<option value="capitals"/>capitals				</font>
<font size="10">
<option value="verse"/>verse				</font>
<font size="10">
<option value="maths"/>maths				</font>
<font size="10">
<option value="victory"/>victory				</font>
<font size="10">
<option value="computers"/>computers				</font>
<font size="10">
<option value="tieknots"/>tieknots				</font>
<font size="10">
<option value="massage"/>massage				</font>
</SELECT>
<INPUT TYPE="hidden" VALUE="true" NAME="switchdeck"  style="height: 3em; width: 20em; font-size: 150%; background-color:gainsboro;">
<input name="referringpage"  value="/stats.php" type="hidden" />
</FORM>

*/
