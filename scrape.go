package gimpledred

import (
	"appengine"
	"appengine/memcache"
	"appengine/taskqueue"
	"appengine/urlfetch"
	"bitbucket.org/pborgeest/gimpledred/surusu"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

/*
type DeckGetter interface {
		FoundLoginForm(client *http.Client) bool
		Login(client *http.Client)
	    GetDecks(client *http.Client) string
	    GetDeckCards(deck string, client *http.Client, retries int) string
}
*/

func cronhandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	keyid, decks, err := loadDecks(c)
	if err != nil {
		c.Errorf("Error storing %v:%v", decks, err)
	}
	loadDecksLater(c, keyid, decks)
}

func fetchdeckhandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	keyid, err := strconv.ParseInt(r.FormValue("keyid"), 0, 64)
	if err != nil {
		c.Errorf("Error parsing keyid %s \n %v", r.FormValue("keyid"), err)
		return
	}
	deck := r.FormValue("deck")
	fmt.Printf("Form data: %v\n", r.Form)
	loadCards(c, keyid, deck)
}

func loadDecksLater(c appengine.Context, keyid int64, decks []string) {
	for _, deck := range decks {
		t := taskqueue.NewPOSTTask("/fetchonedeck", map[string][]string{
			"keyid": {fmt.Sprintf("%d", keyid)},
			"deck":  {deck},
		})
		if _, err := taskqueue.Add(c, t, "decks"); err != nil {
			c.Errorf("Error: %v", err)
		}
	}
}

func loadDecks(c appengine.Context) (int64, []string, error) {
	client := newClient(c)
	if surusu.FoundLoginForm(client) {
		username, err := GetConfig(c, "username")
		if err != nil {
			return 0, []string{}, err
		}
		password, err := GetConfig(c, "password")
		if err != nil {
			return 0, []string{}, err
		}
		surusu.Login(client, username, password)
	}
	decks := surusu.GetDecks(client)

	keyid, err := putTheseDecksInStorage(c, decks)
	return keyid, decks, err
}

func loadCards(c appengine.Context, keyid int64, deck string) {
	if deck == "gardiner" {
		c.Infof("skipping loadCards(%v)\n", deck)
		return
	}
	c.Infof("Running loadCards(%v)\n", deck)
	client := newClient(c)
	if surusu.FoundLoginForm(client) {
		username, err := GetConfig(c, "username")
		if err != nil {
			c.Errorf("Error getting username: %v", err)
		}
		password, err := GetConfig(c, "password")
		if err != nil {
			c.Errorf("Error getting password: %v", err)
		}
		surusu.Login(client, username, password)
	}
	c.Infof("Calling GetDeckCards")
	deckcards, err := surusu.GetDeckCards(deck, client, 28)
	if err != nil {
		c.Errorf("Error storing %v:%v", deck, err)
		return
	}

	if err := putTheseCardsInStorage(c, keyid, deckcards); err != nil {
		c.Errorf("Error storing cards %v:%v", deckcards, err)
	}

	deleteTheViewModelFromCache(c)
	return
}

type Jar struct {
	cookies []*http.Cookie
}

func (jar *Jar) SetCookies(u *url.URL, cookies []*http.Cookie) {
	jar.cookies = cookies
}

func (jar *Jar) Cookies(u *url.URL) []*http.Cookie {
	return jar.cookies
}

func newClient(c appengine.Context) *http.Client {
	jar := new(Jar)
	client := urlfetch.Client(c)
	client.Jar = jar
	client.Transport = &urlfetch.Transport{
		Context:  c,
		Deadline: time.Duration(59) * time.Second,
	}
	return client
}

func deleteTheViewModelFromCache(c appengine.Context) {
	memcache.Delete(c, "viewmodel")
}
