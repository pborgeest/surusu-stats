package surusu

import (
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"sort"
	"strings"

	"bitbucket.org/pborgeest/gimpledred/card"
)

func GetDeckCards(deck string, client *http.Client, limit int) (card.Cards, error) {
	response, err := client.Get("http://surusu.com/stats.php")
	if err != nil {
		return card.Cards{deck, 0, 0}, err
	}

	body := getBody(response, err)
	if !isCorrectDeckLoaded(body, deck) {
        body, err = switchToDeck(client, deck)
        if err != nil {
            return card.Cards{deck, 1, 1}, err
        }
	}

    due, total := parseStats(body)
	return card.Cards{deck, due, total}, nil
}

func switchToDeck(client *http.Client, deck string) (string, error) {
		values := make(url.Values)
		values.Set("selecteddeck", deck)
		values.Set("switchdeck", "true")
		values.Set("referringpage", "/stats.php")
		resp, err := client.PostForm("http://surusu.com/switchdeck.php", values)
		if err != nil {
			return "", err
		}
        body := getBody(resp, err)

		indexresponse, err := client.Get("http://surusu.com/index.php")
		body = getBody(indexresponse, err)
		res, err := client.Get("http://surusu.com/stats.php")
		if err != nil {
			return "", err
		}
		body = getBody(res, err)
        return body, err
}

func isCorrectDeckLoaded(body string, correctDeck string) bool {
	for _, line := range strings.Split(body, "\n") {
    stringmatch, _ := regexp.MatchString(fmt.Sprintf("Current deck: %s\\s*<", correctDeck), line)
		if stringmatch {
			return true
		}
	}
    return false
}

func GetDecks(client *http.Client) []string {
	res, err := client.Get("http://surusu.com/decks.php")
	body := getBody(res, err)

	//foundSwitchdeck, foundFormEnd := false, false
	decks := make([]string, 0)
	for _, line := range strings.Split(body, "\n") {

		if strings.Contains(line, "name=\"newdeckname\"") {
			tagsplit := strings.Split(line, ">")
			righttagsplit := strings.Split(tagsplit[1], "</")
			candidatename := righttagsplit[0]
			if !strings.Contains(candidatename, "surusu.txt") {
				decks = append(decks, candidatename)
			}
		}
	}
	sort.Strings(decks)
	return decks
}
