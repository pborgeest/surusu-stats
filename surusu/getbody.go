package surusu

import (
	"io/ioutil"
	"log"
	"net/http"
)

func getBody(resp *http.Response, err error) string {
	if err != nil {
		log.Fatal(err)
	}
	robots, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	return string(robots)
}
