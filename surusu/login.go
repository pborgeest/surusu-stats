package surusu

import (
	"log"
	"net/http"
	"net/url"
	"strings"
)

func Login(client *http.Client, username string, password string) {
	_, err := client.PostForm(
        "http://surusu.com/login.php",
        url.Values{"username": {username}, "password": {password}},
    )
	if err != nil {
		log.Fatal(err)
	}
}

func FoundLoginForm(client *http.Client) bool {
	res, err := client.Get("http://surusu.com/")
	body := getBody(res, err)
	return strings.Contains(body, "form method=\"POST\" action=\"login.php\"")
}
