package surusu

import (
	"strconv"
	"strings"
)

// types
type cardsdue struct {
    lines []string
    currentline int
    due int
    total int
}

type stateFn func(*cardsdue) stateFn


// This is the entry point.
// `body` is the content of the surusu.com/stats page
// for a previously determined deck of cards
// it parses the page and returns the cards due
// and the total number of cards for that deck.
func parseStats(body string) (int, int) {
    cd := cardsdue{ strings.Split(body, "\n"), 0, 3, 3, }
    cd.run()
    return cd.due, cd.total
}


// state machine
func (cd *cardsdue) run() {
    initialState := notYetFoundCardsDue
    for state := initialState; state != nil; {
        state = state(cd)
    }
}

func notYetFoundCardsDue(cd *cardsdue) stateFn {
    if strings.Contains(cd.lines[cd.currentline], "of cards due today:") {
        return getCardsDueToday
    }
    cd.currentline = cd.currentline + 1
    return notYetFoundCardsDue
}

func getCardsDueToday(cd *cardsdue) stateFn {
    cd.due = getIntBetweenTheBs(cd.lines[cd.currentline])
    return notYetFoundTotalCards
}

func notYetFoundTotalCards(cd *cardsdue) stateFn {
    if strings.Contains(cd.lines[cd.currentline], "of cards in deck:") {
        return getCardsInDeck
    }
    cd.currentline = cd.currentline + 1
    return notYetFoundTotalCards
}

func getCardsInDeck(cd *cardsdue) stateFn {
    cd.total = getIntBetweenTheBs(cd.lines[cd.currentline])
    return nil
}


// helper function
func getIntBetweenTheBs(input string) int {
	firstsplit := strings.Split(input, "<b>")
	secondsplit := strings.Split(firstsplit[1], "</b>")
    prospect := strings.Replace(secondsplit[0], ",", "", -1)
	output, _ := strconv.Atoi(prospect)
	return output
}

