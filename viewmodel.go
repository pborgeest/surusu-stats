package gimpledred

import (
	"sort"
	"fmt"
	"appengine"
)

type ViewModel struct {
	Decknames []string
	Duecardsdata []RowView
	Totalcardsdata []RowView
}

type RowView struct {
	Datename string
	Data []int
}

type SnapWithMap struct {
	snapshot FullSnapshot
	duesnapmap Snapmap
	totalsnapmap Snapmap
}

// Snapmap maps the name of a deck to the number of cards due in the deck
type Snapmap map[string]int

func getViewModel(c appengine.Context, snapshots []FullSnapshot) ViewModel {
	var output = ViewModel{}
	snapswithmaps := convertToSnapwithmaps(c, snapshots)
	output.Decknames = getDeckNames(snapswithmaps)
	output.Duecardsdata = getDueCardsData(output.Decknames, snapswithmaps)
	output.Totalcardsdata = getTotalCardsData(output.Decknames, snapswithmaps)
	return output
}

func convertToSnapwithmaps(c appengine.Context, snapshots []FullSnapshot) []SnapWithMap {
	output := make([]SnapWithMap, len(snapshots))
	for i := 0; i < len(snapshots); i++ {
		thisduemap := make(Snapmap,len(snapshots[i].snapshot.Decks))
		thistotalmap := make(Snapmap,len(snapshots[i].snapshot.Decks))
		for j:=0; j<len(snapshots[i].snapshot.Decks); j++ {
			thisdeck := snapshots[i].snapshot.Decks[j]
			thisduemap[thisdeck] = getThisDecksDueCards(c, snapshots[i].key, thisdeck)
			thistotalmap[thisdeck] = getThisDecksTotalCards(c, snapshots[i].key, thisdeck)
		}
		output[i].snapshot = snapshots[i]
		output[i].duesnapmap = thisduemap
		output[i].totalsnapmap = thistotalmap
	}
	return output
}

// Not guaranteed that all Snapshots will have the same decks
func getDeckNames(snapshots []SnapWithMap) []string {
	nameset := make(Set,0)
	for i := 0; i < len(snapshots); i++ {
		for k,_ := range snapshots[i].duesnapmap {
			nameset.add(k) 
		}
	}

	return nameset.strings()
}

func getDueCardsData(decknames []string, snapmaps []SnapWithMap) []RowView {
	output := make([]RowView,len(snapmaps))
	for i:=0; i<len(snapmaps); i++ {
		inshot := snapmaps[i].snapshot.snapshot
		output[i].Datename = fmt.Sprintf("%s",inshot.Timestamp.Format("2006-01-02"))
		output[i].Data = make([]int,len(decknames))
		for j:=0; j<len(decknames); j++ {
			output[i].Data[j] = snapmaps[i].duesnapmap[decknames[j]]
		}
	}
	return output
}

func getTotalCardsData(decknames []string, snapmaps []SnapWithMap) []RowView {
	output := make([]RowView,len(snapmaps))
	for i:=0; i<len(snapmaps); i++ {
		inshot := snapmaps[i].snapshot.snapshot
		output[i].Datename = fmt.Sprintf("%s",inshot.Timestamp.Format("2006-01-02"))
		output[i].Data = make([]int,len(decknames))
		for j:=0; j<len(decknames); j++ {
			output[i].Data[j] = snapmaps[i].totalsnapmap[decknames[j]]
		}
	}
	return output
}


type Set map[string]bool

func (p *Set) add(k string) {
	set := *p
	set[k] = true
	*p = set
}

func (s Set) strings() []string {
	output := make([]string,0)
	for k,_ := range s {
		output = append(output, k)
	}
	sort.Strings(output)
	return output
}
